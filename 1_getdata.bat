@if "%~1"=="" goto blank
@if "%~2"=="" goto blank
@if "%~3"=="" goto blank

@SET a1=%1%
@SET a2=%2%
@SET a3=%3%
goto startrest

:blank
@echo.
@echo.
@echo I generate the three txt files for output processing.
@echo.
@echo You can pass me drive letter then folder name of the study.
@echo (ie: "getdata y clarkpost")
@echo.
@echo Or, use in interactive mode below.
@echo.

@SET /P a1= "Enter drive letter : "
@SET /P a2= "Enter folder name : "
@SET /P a3= "Enter folder name : "

:startrest

%a1%:
cd \%a2%\e-inst

c:\csm\5.4\layout -o -n -ou=layouton.txt

c:\csm\5.4\output -a -b -ou=outputab.txt

c:\csm\5.4\showhist -n -o -ou=showhist.txt

c:\csm\5.4\showhist -z -ou=zerohist.txt

@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION
(
FOR /f "usebackqdelims=" %%a IN ("layouton.txt") DO (
 SET "line=%%a"
 SET "line=!line:@=_!"
 ECHO !line!
)
)>"layoutclean.txt"

(
FOR /f "usebackqdelims=" %%a IN ("showhist.txt") DO (
 SET "line=%%a"
 SET "line=!line:@=_!"
 ECHO !line!
)
)>"showhistclean.txt"

(
FOR /f "usebackqdelims=" %%a IN ("zerohist.txt") DO (
 SET "line=%%a"
 SET "line=!line:@=_!"
 ECHO !line!
)
)>"zerohistclean.txt"


xcopy /f /y ..\STUDYDEF. %a3%
xcopy /f /y layoutclean.txt %a3%
xcopy /f /y outputab.txt %a3%
xcopy /f /y showhistclean.txt %a3%

xcopy /f /y ..\zeros. %a3%
xcopy /f /y zerohistclean.txt %a3%


cd %a3%
dir /od *.txt
@if NOT "%~1"=="" goto finish
pause
:finish

