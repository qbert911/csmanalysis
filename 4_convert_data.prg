parameters STUDYNAME
if STUDYNAME = FALSE
	STUDYNAME = "thisstudy"
endif

Time1 = time(1)

if _app.databases[ 1 ].tableExists( "apr_out" )
	drop table apr_out
endif
if _app.databases[ 1 ].tableExists( "apr_zero" )
	drop table apr_zero
endif
if _app.databases[ 1 ].tableExists( "apr_zero_forlog" )
	drop table apr_zero_forlog
endif



h = new File()
if h.exists("xls_headers.txt")
   h.delete("xls_headers.txt") 
endif

ee=0
use layout
	go top
	y="("+(val(leng)+1)+")"
	CREATE TABLE apr_out  (caseid CHARACTER &y)
	CREATE TABLE apr_zero (caseid CHARACTER &y)
	CREATE TABLE apr_zero_forlog (junk CHARACTER (3))
   ALTER TABLE "apr_zero_forlog.dbf" ADD caseid CHARACTER &y
   h.create("xls_headers.txt", "A") 
   h.write("caseid")
   lastplace = val(col) +val(leng)
	skip
do
	y="("+leng+")"
	x=trim(text)
	if val(row) == 0
      if val(col) >= lastplace
         lastplace = val(col) +val(leng)
	      try
				ALTER TABLE "apr_zero.dbf" ADD &x CHAR &y
				ALTER TABLE "apr_zero_forlog.dbf" ADD &x CHAR &y
			catch ( Exception e )
	         ee++
   	   	? "ERROR CAUGHT:"+e.message,text,row,col,leng,lastplace
      	endtry
      else
        	? text,row,col,leng,lastplace,"skipped overlapping variable"
      endif
	else
      try
			ALTER TABLE "apr_out.dbf" ADD &x CHAR &y
		catch ( Exception e )
         ee++
      	? "ERROR CAUGHT:"+e.message,text,row,col,leng,lastplace      
      endtry 
  	   h.write(",&x")
	endif
	skip
until EOF()

CREATE INDEX caseid ON apr_zero (caseid)
CREATE INDEX caseid ON apr_zero_forlog (caseid)

use spec_ref
	count to totalrecs
	set order to Ques
  	lastone = "xx"
   skip  //sometimes first row does not get sorted accurately for some reason
do while not EOF()
	x=trim(Ques)+"_so"
	if lastone <> Ques and totalrecs > 0 
      //? "adding",x
      try
			ALTER TABLE "apr_out.dbf" ADD &x CHAR (254)
      	h.write(",&x")
      catch ( Exception e )
         ee++
      	? "ERROR CAUGHT:"+e.message,ques,lastone
      endtry	   

	endif
	lastone = Ques
  	skip
enddo
h.puts(" ")
h.close()
close all

CREATE INDEX caseid ON apr_out (caseid)

//-----------------------------------------------------------------------------

COPY TABLE layout to layoutnozero
use layoutnozero
  dele for val(row) = 0
close all

invd = new query()
invd.sql = 'select * from apr_imp'
invd.active = true
invd.rowset.indexname := "id_rec"

invf = new query()
invf.sql = 'select * from apr_out'
invf.active = true

spec_f = new query()
spec_f.sql = 'select * from "spec_ref.dbf"'
spec_f.active = true
spec_f.rowset.indexname = 'caseid'

//------------------------------------------
invd.rowset.first()
do while not invd.rowset.endofset
   if invd.rowset.fields['recnum'].value = ' 1' AND NOT ("TEST" $ invd.rowset.fields["caseid"].value)
      Mcaseid = invd.rowset.fields["caseid"].value

      invf.rowset.beginAppend()
      invf.rowset.fields["caseid"].value := Mcaseid

//GET MAIN CASES DATA
      use layoutnozero
      	go top
      do
        atext=trim(text)
        arow=right(space(1)+trim(row),2)
        brow=right(space(1)+trim(str(val(row)+1)),2)
        acol=val(col)
        aleng=val(leng)

        if invd.rowset.findkey(Mcaseid+arow)
          if (aleng+acol) > 80
            retvar = substr(invd.rowset.fields["rec"].value,acol,81-acol)
            if invd.rowset.findkey(Mcaseid+brow)
              retvar = retvar + substr(invd.rowset.fields["rec"].value,1,aleng-(81-acol))
            else
              msgbox('no find '+ Mcaseid+brow)
            endif
          else
            retvar = substr(invd.rowset.fields["rec"].value,acol,aleng)
          endif
        else
          msgbox('no find '+ Mcaseid+arow)
        endif
          invf.rowset.fields["&atext"].value = retvar
        skip
      until EOF()

		//GET OUTSIDE SPECIFY DATA
		if spec_f.rowset.findkey(Mcaseid)
			do while spec_f.rowset.fields['caseid'].value == Mcaseid
      		out_field = trim(spec_f.rowset.fields['ques'].value)+"_so"
				invf.rowset.fields["&out_field"].value = spec_f.rowset.fields['text'].value
      	   spec_f.rowset.next()
			enddo
	   endif

      invf.rowset.save()
   endif
   invd.rowset.next()
 enddo
close databases
Time2 = time(1)
? elapsed(Time2, Time1, 1), " seconds to run convert_data"
DROP TABLE layoutnozero
//------------------------------------------

use apr_out
	copy to apr_out.csv delimited     //for data output file
	run "copy xls_headers.txt+apr_out.csv casesdata-forexcel_"&STUDYNAME".csv"
	delete file apr_out.csv
close all

use apr_zero
	append from zeros. sdf  //for living list variables
close databases

   ALTER TABLE "apr_zero_forlog.dbf" ADD enum numeric (2)
   ALTER TABLE "apr_zero_forlog.dbf" ADD lastone boolean
   
use apr_zero_forlog
	append from zerohistclean.txt sdf  //for histlog
close databases


? ee,"fields not created"
