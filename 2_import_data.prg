close all
set safety off
//-----------------------------------------------------------------------------
if _app.databases[ 1 ].tableExists( "layout" )
	drop table layout
endif

CREATE TABLE layout (;
text CHARACTER(28),;
x0type CHARACTER(13),;
row CHARACTER(8),;
col CHARACTER(8),;
leng CHARACTER(8),;
x1 CHARACTER(8),;
x2 CHARACTER(8))

use layout excl
  append from studydef. sdf
  replace row with substr(text,6,2) for left(text,5) == "data="
  calc max(row) to OUTPUTROWSDEF
  zap
  append from layoutclean.txt sdf
  replace text with "a_"+text for not isalpha(left(text,1)) OR trim(text) $ "RNAM/ISEX/DATE/TIME/DAY/MONTH/YEAR/MAX"
  calc max(row) to OUTPUTROWS
  go top
  replace text with "caseid"
  THISidlength=val(leng)
  THISidlengthtxt="("+leng+")"
close all
//-----------------------------------------------------------------------------
if _app.databases[ 1 ].tableExists( "apr_imp" )
	drop table apr_imp
endif

CREATE TABLE apr_imp (;
rec CHARACTER(80),;
recnum CHARACTER(2),;
caseid CHARACTER &THISidlengthtxt)

use apr_imp
  append from outputab.txt sdf
close tables
//-----------------------------------------------------------------------------
OUTPUTROWS = val(max(OUTPUTROWS,OUTPUTROWSDEF)) //sometimes STUYDEF has additional rows not used for data output

invd = new query()
invd.sql = 'select * from apr_imp'
invd.active = true

invd.rowset.first()
invd.rowset.beginEdit()
do while not invd.rowset.endofset
   Mcaseid = SUBSTR(invd.rowset.fields["rec"].value,1,THISidlength)
   for i = 1 to OUTPUTROWS
      invd.rowset.fields["caseid"].value = MCaseid
      invd.rowset.fields["recnum"].value = str(i,2)
      invd.rowset.next()
   next
enddo
invd.rowset.save()

invd.active = false
invd = null

use apr_imp excl
  index on ( caseid + recnum) tag ID_REC
close all
