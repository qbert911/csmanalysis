set safety off

//setup workbook to write to
oExcel = new oleAutoclient("Excel.Application")
//oExcel.workbooks.open("c:\apr14\analysis\apr_living_list.xls")
oExcel.workbooks.open("P:\!NYS APR13 & 14\apr_living_list_ny.xls")
                       
oExcel.Worksheets(1).Select 
/*
oExcel2 = new oleAutoclient("Excel.Application")
oExcel2.workbooks.open("k:\analysis\cases_tocall.xls")
oExcel2.Worksheets(1).Select 

oExcel3 = new oleAutoclient("Excel.Application")
oExcel3.workbooks.open("k:\analysis\cases_appointments.xls")
oExcel3.Worksheets(1).Select 
*/
invf = new query()
invf.sql = 'select * from apr_out'
invf.active = true
invf.rowset.indexname := "caseid"

zer = new query()
zer.sql = 'select * from apr_zero'
zer.active = true
zer.rowset.indexname := "caseid"

wc = new query()
wc.sql = 'select * from "\APPS\aprv2w-analysis\apr_out_forlock.dbf"'
wc.active = true
wc.rowset.indexname= 'caseid'

rowcounter = 1

   col = "A"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "CASEID"
   col = "B"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "COMPLETERS"
   col = "C"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "REFUSALS"
   col = "D"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "NONUMS"
   col = "E"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "PROBLEM"
   col = "F"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "DISCEASED"

	wv("G",rowcounter,"SuperHold")	
	wv("H",rowcounter,"WHOVAR")	

	wv("I",rowcounter,"Int#")	
	wv("J",rowcounter,"Calls Made")	

	wv("K",rowcounter,"Time in Case")	
	wv("L",rowcounter,"Date Last")	
	wv("M",rowcounter,"Time Last")	

	wv("N",rowcounter,"apt1-date")	
	wv("O",rowcounter,"apt1-begin")	
	wv("P",rowcounter,"apt1-end")	

	wv("Q",rowcounter,"apt2-date")	
	wv("R",rowcounter,"apt2-begin")	
	wv("S",rowcounter,"apt2-end")	
/*
   wv("T",rowcounter,'cb5_so')
   wv("U",rowcounter,'ref_so')
   wv("V",rowcounter,'nyqcomm_so')
   wv("W",rowcounter,'thnk_so')

   wv("X",rowcounter,'nyq5_so')
   wv("Y",rowcounter,'nyq6_so')
   wv("Z",rowcounter,'nyq15_so')
   wv("AA",rowcounter,'nyq25_so')
   wv("AB",rowcounter,'nyq31_so')
   wv("AC",rowcounter,'nyq41_so')
   wv("AD",rowcounter,'nyq45_so')





   col = "E"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "CASESSTATUS"
   col = "F"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "SUPERVISOR99"
   col = "G"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "CALLBACK?"
   col = "H"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "CALL-MONTH"
   col = "I"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "CALL-DAY"
   col = "J"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "CALL-TIME"
*/
   col = "T"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "Lname"
   col = "U"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "Fname"
/*
   col = "M"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "School"
   col = "N"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = "Cohort"


   //setup callback list
   col = "A"
	oExcel3.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel3.activecell.formula = "CASEID"
   col = "B"
	oExcel3.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel3.activecell.formula = "CALLBACK?"
   col = "C"
	oExcel3.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel3.activecell.formula = "CALL-MONTH"
   col = "D"
	oExcel3.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel3.activecell.formula = "CALL-DAY"
   col = "E"
	oExcel3.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel3.activecell.formula = "CALL-TIME"
*/

rowcounter = 2
//tocallcounter = 1
//callbackcounter = 2
completecounter = 0
refusedcounter = 0
nonumscounter = 0
probdeccounter= 0

invf.rowset.first()
do while not invf.rowset.endofset

rankvar = 0

	//caseid
	col = "A"
   data = invf.rowset.fields['caseid'].value
   wv(col,rowcounter,data)

   //complete
	col = "B"
	if not empty(invf.rowset.fields['nyq50'].value) AND not empty(invf.rowset.fields['nyq66_a'].value) AND (not empty(invf.rowset.fields['nyq18'].value) OR not empty(invf.rowset.fields['nyQ1'].value) )
	   data = "1"
		completecounter++
   else
		//web completers
      if wc.rowset.findkey(invf.rowset.fields['caseid'].value)
         ? "web complete marked: ",invf.rowset.fields['caseid'].value
		   data = "1"
			completecounter++
      else
	   	data = "0"
      endif
    	//      rankvar++
   endif
   wv(col,rowcounter,data)

   //refusal
	col = "C"
	if (invf.rowset.fields['nyintrob'].value == 2 OR (invf.rowset.fields['cdil'].value == 'R' AND empty(invf.rowset.fields['nyq50'].value));
   															OR invf.rowset.fields['cb'].value == 'y';
                                                OR (not empty(invf.rowset.fields['nyq66_a'].value) AND (empty(invf.rowset.fields['nyq18'].value) AND empty(invf.rowset.fields['nyQ1'].value) )));
						AND NOT (wc.rowset.findkey(invf.rowset.fields['caseid'].value))

	   data = "1"
		refusedcounter++
   else
   	data = "0"
//      rankvar++
   endif
   wv(col,rowcounter,data)

   //nonums
	col = "D"
	if invf.rowset.fields['cdil'].value == 'U'  AND NOT(wc.rowset.findkey(invf.rowset.fields['caseid'].value))
	   data = "1"
		nonumscounter++
   else
   	data = "0"
//      rankvar++
   endif
   wv(col,rowcounter,data)

   //problem
	col = "E"
 	if invf.rowset.fields['cdil'].value == '0' AND NOT (wc.rowset.findkey(invf.rowset.fields['caseid'].value)) AND NOT (not empty(invf.rowset.fields['nyq50'].value) AND not empty(invf.rowset.fields['nyq66_a'].value) AND not empty(invf.rowset.fields['nyq18'].value) )
	   data = "1"
		probdeccounter++
   else
   	data = "0"
//      rankvar++
   endif
   wv(col,rowcounter,data)

   //deceased
	col = "F"
	if invf.rowset.fields['cdil'].value == 'D'
	   data = "1"
		probdeccounter++
   else
   	data = "0"
//      rankvar++
   endif
   wv(col,rowcounter,data)

   //whovar
   wv("H",rowcounter,invf.rowset.fields['whovar'].value)


   //zero record variables - raw for now?
	if zer.rowset.findkey(invf.rowset.fields['caseid'].value)
      
	  	wv("I",rowcounter,zer.rowset.fields['INVW'].value)
	  	wv("J",rowcounter,zer.rowset.fields['TCNT'].value)
      
		wv("K",rowcounter,zer.rowset.fields['QATT'].value)
		wv("L",rowcounter,zer.rowset.fields['LDAT'].value)
		wv("M",rowcounter,zer.rowset.fields['LTIM'].value)

		wv("N",rowcounter,zer.rowset.fields['NDT1'].value)
		wv("O",rowcounter,zer.rowset.fields['NBT1'].value)
		wv("P",rowcounter,zer.rowset.fields['NET1'].value)

		wv("Q",rowcounter,zer.rowset.fields['NDT2'].value)
		wv("R",rowcounter,zer.rowset.fields['NBT2'].value)
		wv("S",rowcounter,zer.rowset.fields['NET2'].value)
      
//superhold
		if not empty(zer.rowset.fields['SHLD'].value)
	   	data = '1'
      else
         data = '0'
      endif
  	 	wv("G",rowcounter,data)
		
   else
  		?invf.rowset.fields['caseid'].value, "NOT FOUND ON ZERO RECORD"
   endif
*/
	//comment fields to print
/*
   wv("T",rowcounter,invf.rowset.fields['cb5_so'].value)
   wv("U",rowcounter,invf.rowset.fields['ref_so'].value)
   wv("V",rowcounter,invf.rowset.fields['nyqcomm_so'].value)
   wv("W",rowcounter,invf.rowset.fields['thnk_so'].value)

   wv("X",rowcounter,invf.rowset.fields['nyq5_so'].value)
   wv("Y",rowcounter,invf.rowset.fields['nyq6_so'].value)
   wv("Z",rowcounter,invf.rowset.fields['nyq15_so'].value)
   wv("AA",rowcounter,invf.rowset.fields['nyq25_so'].value)
   wv("AB",rowcounter,invf.rowset.fields['nyq31_so'].value)
   wv("AC",rowcounter,invf.rowset.fields['nyq41_so'].value)
   wv("AD",rowcounter,invf.rowset.fields['nyq45_so'].value)
*?

   /*
   col = "E"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data0
   col = "F"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data1
*/
/*
   //callback
	if invf.rowset.fields['cb'].value <> "x" and not empty(invf.rowset.fields['cb'].value) AND;
      invf.rowset.fields['nday'].value <> "x" and not empty(invf.rowset.fields['nday'].value)
	   data0 = "callback on: "
      data1 = invf.rowset.fields['cb'].value
      data2 = invf.rowset.fields['nday'].value
      data3 = invf.rowset.fields['rntm'].value

     //if date is already past then put it on the tocall list by incrementing rankvar
     if trim(data1) = "12" or (trim(data1) = "1" and ltrim(str(day(date()))) > trim(data2))
		  rankvar++
     endif


     if rankvar ==4
     	col = "A"
	   data = invf.rowset.fields['caseid'].value
		oExcel3.Range( col+ltrim(str(callbackcounter)) ).Select()
		oExcel3.activecell.formula = data
      col = "B"
		oExcel3.Range( col+ltrim(str(callbackcounter)) ).Select()
		oExcel3.activecell.formula = data0
	   col = "C"
		oExcel3.Range( col+ltrim(str(callbackcounter)) ).Select()
		oExcel3.activecell.formula = data1
	   col = "D"
		oExcel3.Range( col+ltrim(str(callbackcounter)) ).Select()
		oExcel3.activecell.formula = data2
	   col = "E"
		oExcel3.Range( col+ltrim(str(callbackcounter)) ).Select()
		oExcel3.activecell.formula = data3

      callbackcounter++
	  endif
   else
   	data0 = "no callback"
      data1 = ""
      data2 = ""
      data3 = ""
      rankvar++
   endif
   col = "G"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data0
   col = "H"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data1
   col = "I"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data2
   col = "J"
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data3
*/
	//lname
	col = "T"
   data = invf.rowset.fields['lname'].value
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data
	//lname
	col = "U"
   data = invf.rowset.fields['fname'].value
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data
/*
	//lname
	col = "M"
   data = invf.rowset.fields['school'].value
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data
	//lname
	col = "N"
   data = invf.rowset.fields['cohort'].value
	oExcel.Range( col+ltrim(str(rowcounter)) ).Select()
	oExcel.activecell.formula = data



  if rankvar == 5
	col = "A"
   data = invf.rowset.fields['caseid'].value
	oExcel2.Range( col+ltrim(str(tocallcounter)) ).Select()
	oExcel2.activecell.formula = data

	//lname
	col = "E"
   data = invf.rowset.fields['lname'].value
	oExcel2.Range( col+ltrim(str(tocallcounter)) ).Select()
	oExcel2.activecell.formula = data
	//lname
	col = "F"
   data = invf.rowset.fields['fname'].value
	oExcel2.Range( col+ltrim(str(tocallcounter)) ).Select()
	oExcel2.activecell.formula = data
	//lname
	col = "G"
   data = invf.rowset.fields['school'].value
	oExcel2.Range( col+ltrim(str(tocallcounter)) ).Select()
	oExcel2.activecell.formula = data
	//lname
	col = "H"
   data = invf.rowset.fields['cohort'].value
	oExcel2.Range( col+ltrim(str(tocallcounter)) ).Select()
	oExcel2.activecell.formula = data

   tocallcounter++
  endif
*/
   if (rowcounter -2)%100 = 0
		? (rowcounter -2),time()
   	? invf.rowset.fields['caseid'].value
   endif
   rowcounter++
invf.rowset.next()
enddo


/*
for x = tocallcounter to 900
	col = "A"
	oExcel2.Range( col+ltrim(str(x)) ).Select()
	oExcel2.activecell.formula = "           "
	col = "E"
	oExcel2.Range( col+ltrim(str(x)) ).Select()
	oExcel2.activecell.formula = "           "
	col = "F"
	oExcel2.Range( col+ltrim(str(x)) ).Select()
	oExcel2.activecell.formula = "           "
	col = "G"
	oExcel2.Range( col+ltrim(str(x)) ).Select()
	oExcel2.activecell.formula = "           "
	col = "H"
	oExcel2.Range( col+ltrim(str(x)) ).Select()
	oExcel2.activecell.formula = "           "
next x

for x = callbackcounter to 900
	col = "A"
	oExcel3.Range( col+ltrim(str(x)) ).Select()
	oExcel3.activecell.formula = "           "
	col = "B"
	oExcel3.Range( col+ltrim(str(x)) ).Select()
	oExcel3.activecell.formula = "           "
	col = "C"
	oExcel3.Range( col+ltrim(str(x)) ).Select()
	oExcel3.activecell.formula = "           "
	col = "D"
	oExcel3.Range( col+ltrim(str(x)) ).Select()
	oExcel3.activecell.formula = "           "
	col = "E"
	oExcel3.Range( col+ltrim(str(x)) ).Select()
	oExcel3.activecell.formula = "           "
next x
*/



//move cursor to top 
toppem = 1
col = "A"

oExcel.Range( col+ltrim(str(toppem)) ).Select()



//report page
oExcel.Worksheets(2).Select 
leftoff = 0
ltc = "0"
lcc = "0"
lrc = "0"
lnc = "0"
lpc = "0"


do while 1==1 //find our place and capture data from last entry
leftoff++
col = "C"
oExcel.Range( col+ltrim(str(leftoff)) ).Select()
ttc=oExcel.activecell.formula

col = "G"
oExcel.Range( col+ltrim(str(leftoff)) ).Select()
tcc=oExcel.activecell.formula

col = "K"
oExcel.Range( col+ltrim(str(leftoff)) ).Select()
trc=oExcel.activecell.formula

col = "O"
oExcel.Range( col+ltrim(str(leftoff)) ).Select()
tnc=oExcel.activecell.formula

col = "S"
oExcel.Range( col+ltrim(str(leftoff)) ).Select()
tpc=oExcel.activecell.formula


if empty(ttc)
	exit
else
	ltc = ttc
	lcc = tcc
	lrc = trc
	lnc = tnc
   lpc = tpc
endif

enddo
//? leftoff,ltc


col = "A"
data = dttoc(datetime())
wv(col,leftoff,data)

col = "B"
data = "Total Cases:"
wv(col,leftoff,data)

col = "C"
data = rowcounter - 2
wv(col,leftoff,data)

col = "D"
data = "New Cases:"
wv(col,leftoff,data)

col = "E"
data = rowcounter - 2 - val(ltc)
wv(col,leftoff,data)

col = "F"
data = "Total Completes:"
wv(col,leftoff,data)

col = "G"
data = completecounter
wv(col,leftoff,data)

col = "H"
data = "New Completes:"
wv(col,leftoff,data)

col = "I"
data = completecounter - val(lcc)
wv(col,leftoff,data)

col = "J"
data = "Total Refusals:"
wv(col,leftoff,data)

col = "K"
data = refusedcounter
wv(col,leftoff,data)

col = "L"
data = "New Refusals:"
wv(col,leftoff,data)

col = "M"
data = refusedcounter - val(lrc)
wv(col,leftoff,data)
//
col = "N"
data = "Total Nonums:"
wv(col,leftoff,data)

col = "O"
data = nonumscounter 
wv(col,leftoff,data)

col = "P"
data = "New Nonums:"
wv(col,leftoff,data)

col = "Q"
data = nonumscounter - val(lnc)
wv(col,leftoff,data)

col = "R"            
data = "Problems/Deceased:"
wv(col,leftoff,data)

col = "S"
data = probdeccounter 
wv(col,leftoff,data)

col = "T"            
data = "New Problems/Deceased:"
wv(col,leftoff,data)

col = "U"
data = probdeccounter - val(lpc)
wv(col,leftoff,data)


//done writing, save and quit
oExcel.ActiveWorkbook.Save()
oExcel.workbooks.close()
oExcel.quit()
release object oExcel                          
release oExcel 
oExcel = null

/*

oExcel2.ActiveWorkbook.Save()
oExcel2.workbooks.close()
oExcel2.quit()
release object oExcel2
release oExcel2
oExcel2 = null



oExcel3.ActiveWorkbook.Save()
oExcel3.workbooks.close()
oExcel3.quit()
release object oExcel3
release oExcel3
oExcel3 = null
*/

function wv(colin,rowin,datain)
	data=datain
   col=colin
   row=rowin

oExcel.Range( col+ltrim(str(row)) ).Select()
oExcel.activecell.formula = data
return
