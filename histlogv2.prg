clear

use apr_zero_forlog excl
zap
append from zerohistclean.txt sdf
dele for empty(caseid)
dele for caseid = "-----------------"
dele for caseid = "eid"
close all

zlog = new query()
zlog.sql = 'select * from apr_zero_forlog'
zlog.active = true

lastid = "0"

zlog.rowset.first()
do while not zlog.rowset.endofset
	thecaseid= zlog.rowset.fields['caseid'].value

   if thecaseid == lastid
		enum++	
	else
   	enum = 0
	endif

	zlog.rowset.beginEdit()
   	zlog.rowset.fields['enum'].value = enum
	zlog.rowset.save()

   lastid = thecaseid

zlog.rowset.next()
enddo

zlog = NULL

use apr_zero_forlog excl
	dele for enum = 0
close all


///////////////////////////////////////////////////////////////////////////

use histlog excl
zap
close all


countskipped =0
a=0
b=0

zlog = new query()
zlog.sql = 'select * from apr_zero_forlog'
zlog.active = true

hlog = new query()
hlog.sql = 'select * from histlog'
hlog.active = true
hlog.rowset.indexname := "caseid"


invf = new query()
invf.sql = 'select * from apr_out'
invf.active = true
invf.rowset.indexname := "caseid"

thisskips = 0

zlog.rowset.first()
do while not zlog.rowset.endofset

   if not(substr(zlog.rowset.fields['caseid'].value,1,16) == lastid) AND (zlog.rowset.fields['enum'].value - thisskips) <> 1
   	thisskips = 0
	endif


	if empty(zlog.rowset.fields['track'].value ) OR zlog.rowset.fields['track'].value = NULL //+;
   			zlog.rowset.fields['mon'].value  +;	
            zlog.rowset.fields['aday'].value +;
            zlog.rowset.fields['ayear'].value +;
            zlog.rowset.fields['atime'].value)
      ?substr(zlog.rowset.fields['caseid'].value,1,16), "skipped" 
		countskipped++
      thisskips++
     
   	zlog.rowset.next()
      loop
	endif
	Mcaseid = substr(zlog.rowset.fields['caseid'].value,1,16)



	wherenum = zlog.rowset.fields['enum'].value - thisskips
//   if Mcaseid = "0000000012VT0288"
//   	?Mcaseid, wherenum, zlog.rowset.fields['enum'].value , thisskips,lastid,"x"+zlog.rowset.fields['track'].value+"x"
//	endif
	whereput = "a"+rtrim(ltrim(str(wherenum)))
	trackvar = zlog.rowset.fields['track'].value 
	


/*
  D - deceased
  U - Nonums
  1 - proceed with interview
  14 - callback
  3 - busy
  R - refusal
  0 - probelm - supervisor
  60 - SA student not home
  2 - no answer
*/
do case
	case trackvar == "D"
   	longtrack = "Deceased"
	case trackvar == "U"
   	longtrack = "Nonums"
	case trackvar == "1"
   	longtrack = "Interview?"
	case trackvar == "14"
   	longtrack = "Callback"
	case trackvar == "3"
   	longtrack = "Busy"
	case trackvar == "R"
   	longtrack = "Refusal"
	case trackvar == "O"
   	longtrack = "Problem"
	case trackvar == "60"
   	longtrack = "Student Not Home"
	case trackvar == "2"
   	longtrack = "No Answer"
   case trackvar == null
   	longtrack = ""
  	otherwise
   	longtrack = "XXXXXXXX"
      ? Mcaseid, "no track info","x"+trackvar+"x"
endcase


   
	
//	? "x"+Mcaseid+"x",whereput,string

   if hlog.rowset.findkey(Mcaseid)
     //	? "found"
	   hlog.rowset.beginEdit()
	else
		hlog.rowset.beginAppend()
   		hlog.rowset.fields['caseid'].value = Mcaseid
   endif

     if invf.rowset.findkey(Mcaseid)
         llstatus = ""
         //determine living list status
			if not empty(invf.rowset.fields['nyQ50'].value) AND not empty(invf.rowset.fields['nyQ66_a'].value) 
				llstatus = "complete"
			else if invf.rowset.fields['nyintrob'].value == 2 OR invf.rowset.fields['cdil'].value == 'R';
   															OR invf.rowset.fields['cb'].value == 'y'
				llstatus = "refused"
			else if invf.rowset.fields['cdil'].value == 'U'
				llstatus = "nonums"
         else if invf.rowset.fields['cdil'].value == '0'
				llstatus = "problem"
			else if invf.rowset.fields['cdil'].value == 'D'
				llstatus = "disceased"
				?"DISCEASED",Mcaseid
			endif

   		//solidify longtrack with apr_out
         if longtrack = "Interview?"
            a++
            if not empty(invf.rowset.fields['nyQ50'].value) AND not empty(invf.rowset.fields['nyQ66_a'].value) 
					longtrack = "Interview!"
               b++
         	else if not empty(invf.rowset.fields['nyQ1'].value) OR not empty(invf.rowset.fields['nyQ18'].value)
					longtrack = "Int.Started"
            	?Mcaseid,longtrack
				else                                                                                              
					longtrack = "Int.Not Started"
            	?Mcaseid,longtrack
				endif
      	endif


			hlog.rowset.fields['llstatus'].value = llstatus
		else
   		? Mcaseid,"NOT FOUND IN APR OUT"
		endif




	string = longtrack+ "-"+;
   			zlog.rowset.fields['mon'].value + "/"+;	
            zlog.rowset.fields['aday'].value+ "/" +;
            zlog.rowset.fields['ayear'].value+ "@" +;
            zlog.rowset.fields['atime'].value 
            

	      //?whereput
		  	hlog.rowset.fields['lastone'].value = longtrack
		  	hlog.rowset.fields['&whereput'].value = string
		hlog.rowset.save()


   lastid = Mcaseid
zlog.rowset.next()
enddo

? countskipped,"skipped"
?a,b, "<-- completers, compare with living list count"
///////////////////////////////////////////////
/*

think about cleaning up diseasced cases
*/
