clear

//---------------------------------------------
use layout excl
  go top
  THISidlength=val(leng)+1
  THISidlengthtxt="("+leng+")"
close all

if _app.databases[ 1 ].tableExists( "histlog" )
	drop table histlog
endif

CREATE TABLE histlog (;
caseid CHARACTER &THISidlengthtxt,;
llstatus CHARACTER(13),;
lastone CHARACTER(31),;
a1 CHARACTER(31),;
a2 CHARACTER(31),;
a3 CHARACTER(31),;
a4 CHARACTER(31),;
a5 CHARACTER(31),;
a6 CHARACTER(31),;
a7 CHARACTER(31),;
a8 CHARACTER(31),;
a9 CHARACTER(31),;
a10 CHARACTER(31),;
a11 CHARACTER(31),;
a12 CHARACTER(31),;
a13 CHARACTER(31),;
a14 CHARACTER(31),;
a15 CHARACTER(31),;
a16 CHARACTER(31),;
a17 CHARACTER(31),;
a18 CHARACTER(31),;
a19 CHARACTER(31),;
a20 CHARACTER(31))

//add histlog table building code here later
//------------------------------------------------
use apr_zero_forlog excl
	dele for empty(caseid)
	dele for caseid = "-----------------"
	dele for caseid = "eid"
close all

zlog = new query()
zlog.sql = 'select * from apr_zero_forlog'
zlog.active = true

lastid = "0"

zlog.rowset.first()
do while not zlog.rowset.endofset
	thecaseid= zlog.rowset.fields['caseid'].value

   if thecaseid == lastid
		enum++	
	else
   	enum = 0
	endif

	zlog.rowset.beginEdit()
   	zlog.rowset.fields['enum'].value = enum
	zlog.rowset.save()

   lastid = thecaseid

zlog.rowset.next()
enddo

zlog = NULL

use apr_zero_forlog excl
	//count for enum = 0
	dele for enum = 0
close all

/*
//----------------------------------------------------------------------------

use histlog excl
zap
close all


countskipped =0
a=0
b=0

zlog = new query()
zlog.sql = 'select * from apr_zero_forlog'
zlog.active = true

hlog = new query()
hlog.sql = 'select * from histlog'
hlog.active = true
hlog.rowset.indexname := "caseid"

invf = new query()
invf.sql = 'select * from apr_out'
invf.active = true
invf.rowset.indexname := "caseid"

thisskips = 0

zlog.rowset.first()
do while not zlog.rowset.endofset

   if not(substr(zlog.rowset.fields['caseid'].value,1,16) == lastid) AND (zlog.rowset.fields['enum'].value - thisskips) <> 1
   	thisskips = 0
	endif

/*
	if empty(zlog.rowset.fields['track'].value ) OR zlog.rowset.fields['track'].value = NULL //+;
   			zlog.rowset.fields['mon'].value  +;	
            zlog.rowset.fields['aday'].value +;
            zlog.rowset.fields['ayear'].value +;
            zlog.rowset.fields['atime'].value)
      ?substr(zlog.rowset.fields['caseid'].value,1,16), "skipped" 
		countskipped++
      thisskips++
     
   	zlog.rowset.next()
      loop
	endif
//
	Mcaseid = substr(zlog.rowset.fields['caseid'].value,1,16)



	wherenum = zlog.rowset.fields['enum'].value - thisskips
//   if Mcaseid = "0000000012VT0288"
//   	?Mcaseid, wherenum, zlog.rowset.fields['enum'].value , thisskips,lastid,"x"+zlog.rowset.fields['track'].value+"x"
//	endif
	whereput = "a"+rtrim(ltrim(str(wherenum)))
//	trackvar = zlog.rowset.fields['track'].value 
	codevar = zlog.rowset.fields['code'].value 
	codevar = zlog.rowset.fields['genl'].value 
	codevar = zlog.rowset.fields['dial'].value 
	


/*
  D - deceased
  U - Nonums
  1 - proceed with interview
  14 - callback
  3 - busy
  R - refusal
  0 - probelm - supervisor
  60 - SA student not home
  2 - no answer
//
do case
	case codevar == "01"
	case codevar == "60"
   	longtrack = "No Answer"
	
	case codevar == "90"
   	longtrack = "Nonums"
//   	longtrack = "Deceased"
   	longtrack = "Problem"//?
	case codevar == "83"
	case codevar == "84"
	case codevar == "62"
	case codevar == "63"
	case codevar == "20"
	case empty(codevar)
   	longtrack = "Interview?"



	case cdilvar == "R"
   	longtrack = "Refusal"
	case cdilvar == "3"
   	longtrack = "Busy"
	case cdilvar == "60"
   	longtrack = "Student Not Home"
	case cdilvar == "14"
   	longtrack = "Callback"
   case cdilvar == null
   	longtrack = ""
      ? Mcaseid, "CDIL empty","x"+cdilvar+"x"+codevar+"x"
  	otherwise
   	longtrack = "XXXXXXXX"
      ? Mcaseid, "no track info","x"+cdilvar+"x"+codevar+"x"
endcase


   
	
//	? "x"+Mcaseid+"x",whereput,string

   if hlog.rowset.findkey(Mcaseid)
     //	? "found"
	   hlog.rowset.beginEdit()
	else
		hlog.rowset.beginAppend()
   		hlog.rowset.fields['caseid'].value = Mcaseid
   endif

     if invf.rowset.findkey(Mcaseid)
         llstatus = ""
         //determine living list status
			if not empty(invf.rowset.fields['nyQ50'].value) AND not empty(invf.rowset.fields['nyQ66_a'].value) 
				llstatus = "complete"
			else if invf.rowset.fields['nyintrob'].value == 2 OR invf.rowset.fields['cdil'].value == 'R';
   															OR invf.rowset.fields['cb'].value == 'y'
				llstatus = "refused"
			else if invf.rowset.fields['cdil'].value == 'U'
				llstatus = "nonums"
         else if invf.rowset.fields['cdil'].value == '0'
				llstatus = "problem"
			else if invf.rowset.fields['cdil'].value == 'D'
				llstatus = "disceased"
				?"DISCEASED",Mcaseid
			endif

   		//solidify longtrack with apr_out
         if longtrack = "Interview?"
            a++
            if not empty(invf.rowset.fields['nyQ50'].value) AND not empty(invf.rowset.fields['nyQ66_a'].value) 
					longtrack = "Interview!"
               b++
         	else if not empty(invf.rowset.fields['nyQ1'].value) OR not empty(invf.rowset.fields['nyQ18'].value)
					longtrack = "Int.Started"
            	?Mcaseid,longtrack
				else                                                                                              
					longtrack = "Int.Not Started"
            	?Mcaseid,longtrack
				endif
      	endif


			hlog.rowset.fields['llstatus'].value = llstatus
		else
   		? Mcaseid,"NOT FOUND IN APR OUT"
		endif

	string = longtrack+ "-"+;
   			zlog.rowset.fields['mon'].value + "/"+;	
            zlog.rowset.fields['aday'].value+ "/" +;
            zlog.rowset.fields['ayear'].value+ "@" +;
            zlog.rowset.fields['atime'].value + "interviewer: "+zlog.rowset.fields['invw'].value 
            

	      //?whereput
		  	hlog.rowset.fields['lastone'].value = longtrack
		  	hlog.rowset.fields['&whereput'].value = string
		hlog.rowset.save()


   lastid = Mcaseid
zlog.rowset.next()
enddo

? countskipped,"skipped"
?a,b, "<-- completers, compare with living list count"
///////////////////////////////////////////////
/*

think about cleaning up diseasced cases
*/


