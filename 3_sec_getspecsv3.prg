set safety off
//-----------------------------------------------------------------------------
if _app.databases[ 1 ].tableExists( "spec_raw" )
	drop table spec_raw
endif
if _app.databases[ 1 ].tableExists( "spec_ref" )
	drop table spec_ref
endif

use layout
	go top
	THISidlengthtxt="("+leng+")"
close tables

CREATE TABLE spec_raw (;
caseid CHARACTER &THISidlengthtxt,;
spacer CHARACTER(1),;
text CHARACTER(254),;
text2 CHARACTER(254),;
text3 CHARACTER(254))

CREATE TABLE spec_ref (;
caseid CHARACTER &THISidlengthtxt,;
Ques CHARACTER(20),;
text CHARACTER(254))

Time1 = time(1)

? "FILE DATE: "+new File().date( "showhistclean.txt" )

use spec_raw
append from showhistclean.txt sdf
	dele for empty(caseid)
	dele for caseid = "---"
	dele for caseid = "Caseid"
  	dele for text ="Text (value="
	dele for text = " //"
	dele for text = "//"
	dele for text = "---"
close databases

spec_w = new query()
spec_w.sql = 'select * from "spec_raw.dbf"'
spec_w.active = true

spec_f = new query()
spec_f.sql = 'select * from "spec_ref.dbf"'
spec_f.active = true

theitemis ="xxxx"
breakpoint = -1
lastone = "xxxx"

spec_w.rowset.first()
do while not spec_w.rowset.endofset

if spec_w.rowset.fields['caseid'].value = "Item"
		theitemis = trim(substr(spec_w.rowset.fields['caseid'].value,6,40)) + trim(spec_w.rowset.fields['spacer'].value) +trim(spec_w.rowset.fields['text'].value)
   if spec_w.rowset.fields['spacer'].value == NULL
		theitemis = trim(substr(spec_w.rowset.fields['caseid'].value,6,40))
	elseif spec_w.rowset.fields['text'].value == NULL
 		theitemis = trim(substr(spec_w.rowset.fields['caseid'].value,6,40)) + trim(spec_w.rowset.fields['spacer'].value)
   endif
	breakpoint = -1
   ? theitemis
else
	if breakpoint == -1				// calculate the number of leading junk characters
		foundcolons = 0
		targetcolons = 3
		if substr(spec_w.rowset.fields['text'].value,1,2) = "os"
			targetcolons = 4
		endif
		for countchar = 1 to 30
			if substr(spec_w.rowset.fields['text'].value,countchar,1) = ":"
				foundcolons++
				if foundcolons = targetcolons
					breakpoint = countchar + 1
					exit
				endif
			endif
		endfor
	endif

	//write data
	if spec_w.rowset.fields['caseid'].value = lastone
		spec_f.rowset.beginEdit()
		newoutput = trim(spec_f.rowset.fields['text'].value) + space(1) + substr(trim(spec_w.rowset.fields['text'].value),breakpoint,254)
		if not empty(spec_w.rowset.fields['text2'].value) OR len(newoutput) > 254
			spec_f.rowset.fields['text'].value = substr(newoutput,1,230)+"FOR MORE SEE BEN"
		else
			spec_f.rowset.fields['text'].value = newoutput
		endif
	else
		spec_f.rowset.beginAppend()
		spec_f.rowset.fields['caseid'].value = spec_w.rowset.fields['caseid'].value
		spec_f.rowset.fields['ques'].value = theitemis
		if not empty(spec_w.rowset.fields['text2'].value)
			spec_f.rowset.fields['text'].value = substr(trim(spec_w.rowset.fields['text'].value),breakpoint,230)+"FOR MORE SEE BEN"
		else
			spec_f.rowset.fields['text'].value = substr(trim(spec_w.rowset.fields['text'].value),breakpoint,254)
		endif
	endif
	spec_f.rowset.save()
	lastone = spec_w.rowset.fields['caseid'].value
endif
spec_w.rowset.next()
enddo

//remove some junk lines
spec_f.active = false
use spec_ref excl
	dele for text == " //"
	dele for text == "//"
pack
close all

CREATE INDEX caseid ON spec_ref (caseid)
CREATE INDEX Ques ON spec_ref (Ques)

Time2 = time(1)
? elapsed(Time2, Time1, 1), " seconds to run sec_getspecsv2"
return
